package com.deploy.deploy.controller;

import com.deploy.deploy.entity.UserDetails;
import com.deploy.deploy.model.UserLoginModel;
import com.deploy.deploy.repo.UserDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeployController {

    @Autowired
    private UserDetailsRepo repo;

    @PostMapping("/api/login")
    @ResponseBody
    public UserDetails login(@RequestBody UserLoginModel userLoginModel) {
        UserDetails userDetails = repo.findByEmailAndPassword(userLoginModel.getEmail(),userLoginModel.getPassword());
        if(userDetails == null){
            throw new RuntimeException();
        }
        return userDetails;
    }

}
