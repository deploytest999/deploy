import { Component } from '@angular/core';
import { logging } from 'protractor';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  email;
  password;
  userdetails;
  isValidUser = true;

  constructor(private service: AppService){}

  submit(){
    this.isValidUser = true;
    this.userdetails = null;
    let data = {
      email: this.email,
      password: this.password
    }
    this.service.login(data).subscribe(res => {
      this.email = "";
      this.password = "";
      this.userdetails = res;
    },err => {
      this.isValidUser = false;
    });
  }

}
