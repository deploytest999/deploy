package com.deploy.deploy.repo;

import com.deploy.deploy.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailsRepo extends JpaRepository<UserDetails,Integer> {

    UserDetails findByEmailAndPassword(String email, String password);
}
